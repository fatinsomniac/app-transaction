<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use Auth;

class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }    
    public function index()
    {
        return view('karyawan.monitoring');
    }
    public function create()
    {
        return view('karyawan.create');
    }
    public function store(Request $request)
    {
        $kirim = $request->all();
        
        
        $file = $request->file('file') ? $request->file('file')->store("images/transactions") : null;
        $kirim['file'] = $file;

        $transaction = auth()->user()->transactions()->create($kirim);    
        return redirect('karyawan.monitoring'); 
    }
    public function viewTransaction()
    {
        $read = Transaction::all();
        return view('karyawan.monitoring', compact('read'));
    }
}
