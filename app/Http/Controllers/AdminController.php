<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construc()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.dashboard');
    }

    public function create()
    {
        return view('admin.create');
    }

    public function store(Request $request)
    {
        $send = $request->all();
        
        $file = $request->file('file') ? $request->file('file')->store("images/transactions") : null;
        $send['file'] = $file;

        $transaction = auth()->user()->transactions()->create($send);    
        return redirect('admin.dashboard'); 
    }

    public function viewAdmin()
    {
        $admin = Transaction::all();
        return view('admin.dashboard', compact('admin'));
    }
}
