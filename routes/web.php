<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/monitoring', 'KaryawanController@index')->middleware('auth');

Route::get('karyawan/create', 'KaryawanController@create')->middleware('auth');
Route::get('karyawan/monitoring', 'KaryawanController@viewTransaction')->middleware('auth');
Route::post('transactions/store','KaryawanController@store')->middleware('auth');


//==========================================================================

Route::get('/admin', 'AdminController@index')->middleware('auth');

Route::get('admin/create', 'AdminController@create')->middleware('auth');
Route::post('admin/store', 'AdminController@store')->middleware('auth');