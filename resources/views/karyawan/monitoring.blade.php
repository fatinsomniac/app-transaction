<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>App Transaction</title>
    <link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('style/style.css')}}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">CRUD</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/karyawan/monitoring">Monitoring</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="/karyawan/create">Create Transaction</a>
            </li>
            <li class="nav-item">
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">    
                  @csrf
                  </form>                 
              </li>
          </li>
          </ul>
        </div>
      </nav>
    <div class="container mt-5">
        <h1>Transaction List</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Karyawan</th>
                    <th>Total Harga</th>
                    <th width="1%">File Bukti</th>
                    <th>Status</th>
                    <th width="1%">Action</th>
                </tr>
            </thead>
            <tbody>
                  @foreach($read as $g)
                  <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$g->name_employees}}</td>
                  <td>{{$g->price}}</td>                  
                  <td><img width="150px" src="{{ asset("storage/" . $g->file) }}"></td>
                  <td>{{$g->status}}</td>
                      <td>
                        <a href="">Edit</a>
                        
                      </td>
                  </tr>
                  @endforeach
            </tbody>
        </table>
    </div>  
</body>
</html>