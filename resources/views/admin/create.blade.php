<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App Transaction</title>
    <link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('style/style.css')}}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">CRUD</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            @if (auth()->user()->role == "admin")
              <li class="nav-item active">
                <a class="nav-link" href="/admin">Monitoring</a>
              </li>
            @else
              <li class="nav-item active">
                <a class="nav-link" href="/monitoring">Monitoring</a>
              </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" href="/admin/create">Create Transaction</a>
            </li>
            <li class="nav-item">
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                  </form>
                </li>
            </li>
          </ul>
        </div>
      </nav>
    <div class="container mt-5" style="width: 33%;">
      <form class="mb-4" action="/admin/store" method="post" enctype="multipart/form-data">
        @csrf
          <h1 class="text-center mb-4">Create Transaction</h1>
          <div class="form-group">
              <label for="">Nama Karyawan</label>
              <input type="text" class="form-control" name="name_employees">
          </div>
          <div class="form-group">
              <label for="">Total Harga</label>
              <input type="number" class="form-control" name="price">
          </div>
          <div class="form-group">
              <label for="">File Bukti</label>
              <input type="file" name="file" class="form-control" name="file">
          </div>
          <div class="form-group">
            <label for="">Status</label>
            <input type="text" class="form-control" name="status">
        </div>
          <button type="submit" id="btn-submit" class="btn btn-primary mt-3">Submit</button>
      </form>
    </div>
<script src="{{asset('scripts/jquery-3.5.0.min.js')}}"></script>
<script src="{{asset('scripts/bootstrap.min.js')}}"></script>
</body>
</html>